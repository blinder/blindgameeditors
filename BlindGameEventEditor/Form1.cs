﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;
using BlindGameDataDefs;
using CSVParser;
using System.Collections.Concurrent;

namespace BlindGameEventEditor
{
    public partial class Form1 : Form
    {
        public List<EventAction> ActionList;
        public EventAction CurrentAct;

        private static string CSVFileForParamDescription = "../../ParamsDesByEActionType.csv";
        private static string[] csvhead = new string[] { "ActionType", "Duration", "Saying", "ActorIdx", "TargetIdx" };

        private bool bSelecting = false;
        private ConcurrentDictionary<eActionType, List<string>> ActionTypeMapToPararmDes = new ConcurrentDictionary<eActionType, List<string>>();
        

        public Form1()
        {
            InitializeComponent();
            comboBoxActionType.Items.Add(eActionType.EAT_DIALOG);
            comboBoxActionType.Items.Add(eActionType.EAT_MOVETO);
            comboBoxActionType.Items.Add(eActionType.EAT_TURNTO);
            comboBoxActionType.Items.Add(eActionType.EAT_IMG);
            comboBoxActionType.Items.Add(eActionType.ESOP_PLACEOUTVILLAGEACTIVITY);
            comboBoxActionType.Items.Add(eActionType.ESOP_INSTANTIATE);
            comboBoxActionType.Items.Add(eActionType.ESOP_ROOMSPOTPENDING);
            comboBoxActionType.Items.Add(eActionType.ESOP_TOGGLEACTIVITIES);
            ActionList = new List<EventAction>();
            CurrentAct = new EventAction();

            if(!File.Exists(CSVFileForParamDescription))
            {
                CsvWriter writer = new CsvWriter();

               foreach (var item in comboBoxActionType.Items)
               {
                    writer.AddRow(new string[] { item.ToString(), "", "", "", "" });
               }
                writer.AddRow(new string[] { "ActionType", "Duration", "Saying", "ActorIdx", "TargetIdx" });
                string csv = writer.Write();
                File.WriteAllText(CSVFileForParamDescription, csv);
            }
            else
            {
                var reader = new CsvReader();
                string csv = File.ReadAllText(CSVFileForParamDescription);
                List<string> rowstr;
                IEnumerable<string[]>  rows = reader.Read(csv);
                //foreach (var row in reader.Read(csv))
                for (int i = 0; i < rows.Count() - 1; ++i) 
                {
                    // so something with the whole row
                    //
                    rowstr = new List<string>();
                    foreach (var cell in rows.ElementAt(i)) 
                    {
                        rowstr.Add(cell);
                        // do something with the cell
                    }
                    if(!ActionTypeMapToPararmDes.TryAdd((eActionType)comboBoxActionType.Items[i], rowstr))
                        Console.WriteLine("TryAdd sucks");
                }
            }
        }

        private void ClearAll()
        {
            ActionList = new List<EventAction>();
            CurrentAct = new EventAction();
            ListBoxAction.ClearSelected();
            ListBoxAction.Items.Clear();
            UpdateListBox();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ddd");
        }

        private void buttonAddAction_Click(object sender, EventArgs e)
        {
            if (ListBoxAction.SelectedItem != null)
                ActionList.Insert(ListBoxAction.SelectedIndex, ActionClone(CurrentAct));
            else
                ActionList.Add(ActionClone(CurrentAct));

            UpdateListBox();
        }

        private void ListBoxAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListBoxAction.SelectedItem == null)
                return;

            CurrentAct = ActionList[ListBoxAction.SelectedIndex];

            bSelecting = true;
            comboBoxActionType.SelectedIndex = CurrentAct.actiontype;
            textBoxActorIdx.Text = CurrentAct.ActorIndex.ToString();
            textBoxDuration.Text = CurrentAct.duration.ToString("f2");
            textBoxSaying.Text = CurrentAct.saying;
            textBoxTargetIdx.Text = CurrentAct.TargetIndex.ToString();
            bSelecting = false;
            
        }

        private void comboBoxActionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            Console.WriteLine(sender.ToString() + " " + ((eActionType)comboBoxActionType.SelectedItem).ToString());
            CurrentAct.actiontype = (int)comboBoxActionType.SelectedItem;
            List<string> line = new List<string>();
            if (!ActionTypeMapToPararmDes.TryGetValue(((eActionType)comboBoxActionType.SelectedItem) , out line))
                Console.WriteLine("No Val found in ActionTypeMapToPararmDes");
                //ActionType	Duration	Saying	ActorIdx	TargetIdx
            
            foreach(var val in ActionTypeMapToPararmDes.Values)
            {
                Console.WriteLine(val[1]);
            }
            LabelActionType.Text = line[0];
            labelDuration.Text= line[1];
            labelSaying.Text= line[2];
            labelActorIdx.Text= line[3];
            labelTarget.Text= line[4];
            
            UpdateListBox();
        }

        private void textBoxDuration_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            Console.WriteLine(sender.ToString() + " " + ListBoxAction.SelectedIndex);
            CurrentAct.duration = float.Parse(textBoxDuration.Text);
            UpdateListBox();
        }

        private void textBoxSaying_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            Console.WriteLine(sender.ToString() + " " + ListBoxAction.SelectedIndex);
            CurrentAct.saying = textBoxSaying.Text;
            UpdateListBox();
        }

        private void textBoxActorIdx_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            Console.WriteLine(sender.ToString() + " " + ListBoxAction.SelectedIndex);
            CurrentAct.ActorIndex = Convert.ToInt32(textBoxActorIdx.Text);
            UpdateListBox();
        }

        private void UpdateListBox(bool clearAll=true)
        {
            String str="";

            if(clearAll)
                ListBoxAction.Items.Clear();

            foreach(EventAction actitr in ActionList)
            {
                switch((eActionType)actitr.actiontype)
                {
                    case eActionType.EAT_DIALOG:
                        str = actitr.ActorIndex.ToString() + "speaks to " + actitr.TargetIndex + " : " + actitr.saying;
                        break;
                    case eActionType.EAT_MOVETO:
                        str = actitr.ActorIndex.ToString() + " move to " + actitr.TargetIndex;
                        break;
                    case eActionType.EAT_IMG:
                        str = " show img " + actitr.saying;
                        break;
                    case eActionType.ESOP_ROOMSPOTPENDING:
                        str = "set spot" + ((eRoomSpot)actitr.ActorIndex).ToString() + " as " + actitr.saying;
                        break;
                    case eActionType.ESOP_TOGGLEACTIVITIES:
                        str = "set activity" + (actitr.saying) + " " +(actitr.TargetIndex == 1 ? "true" : "false");
                        break;
                }
                ListBoxAction.Items.Add(str);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilePath = "";
            SaveFileDialog sfd = new SaveFileDialog();
            //sfd.Filter = "office files(*.doc)|*.doc|Text files(*.txt)|*.txt";
            sfd.Filter = "Blind Game Event Files(*.bev)|*.bev";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径

                
                FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);

                sw.Write(GenerateJson());
                sw.Close();
                //获取文件路径，不带文件名 
                //FilePath = localFilePath.Substring(0, localFilePath.LastIndexOf("\\")); 

                //给文件名前加上时间 
                //newFileName = DateTime.Now.ToString("yyyyMMdd") + fileNameExt; 

                //在文件名里加字符 
                //saveFileDialog1.FileName.Insert(1,"dameng"); 

                //System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();//输出文件 

                ////fs输出带文字或图片的文件，就看需求了 

                fs.Close();
            }
        }

        private string GenerateJson()
        {
            Console.WriteLine("Start GenerateJson==================================");
            string jsonstr;
            StringBuilder GeneratedJsonStr;

            GeneratedJsonStr = new StringBuilder();
            GeneratedJsonStr.Insert(0, "[");

            foreach(EventAction aitr in ActionList)
            {
                jsonstr = JsonMapper.ToJson(aitr);
                GeneratedJsonStr.Append(jsonstr);
                if (aitr != ActionList.Last())
                    GeneratedJsonStr.Append(",");
            }
            GeneratedJsonStr.Insert(GeneratedJsonStr.Length, "]");

            Console.WriteLine(GeneratedJsonStr.ToString());
            Console.WriteLine("End GenerateJson==================================");
            
            return GeneratedJsonStr.ToString();
        }

        public EventAction ActionClone(EventAction CloneTarget)
        {
            EventAction ActionDolly;
            ActionDolly = new EventAction();
            ActionDolly.actiontype = CloneTarget.actiontype;
            ActionDolly.saying = CloneTarget.saying;
            ActionDolly.duration = CloneTarget.duration;
            ActionDolly.ActorIndex = CloneTarget.ActorIndex;
            ActionDolly.TargetIndex = CloneTarget.TargetIndex;

            return ActionDolly;
        }

        private void textBoxTargetIdx_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentAct.TargetIndex = Convert.ToInt32(textBoxTargetIdx.Text);
            UpdateListBox();
        }

        private void buttonRemoveAction_Click(object sender, EventArgs e)
        {
            if(ListBoxAction.SelectedItem!=null)
            {
                ActionList.RemoveAt(ListBoxAction.SelectedIndex);
                UpdateListBox();
            }
        }

        private void InputNumericCheck_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ClearAll();
            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK) 
            {
                FileStream fs = (System.IO.FileStream)ofd.OpenFile();
                StreamReader sr = new StreamReader(fs, Encoding.Default);
                string str = sr.ReadToEnd();
                sr.Close();
                fs.Close();
                Console.Write(str);
                ActionList = JsonMapper.ToObject<List<EventAction>>(str);
                UpdateListBox();
            }
        }
    }
}
