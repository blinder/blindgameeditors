﻿namespace BlindGameEventEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTargetIdx = new System.Windows.Forms.TextBox();
            this.textBoxActorIdx = new System.Windows.Forms.TextBox();
            this.textBoxSaying = new System.Windows.Forms.TextBox();
            this.textBoxDuration = new System.Windows.Forms.TextBox();
            this.labelTargetIdx = new System.Windows.Forms.Label();
            this.buttonRemoveAction = new System.Windows.Forms.Button();
            this.buttonAddAction = new System.Windows.Forms.Button();
            this.comboBoxActionType = new System.Windows.Forms.ComboBox();
            this.ActorIdx = new System.Windows.Forms.Label();
            this.Saying = new System.Windows.Forms.Label();
            this.Duration = new System.Windows.Forms.Label();
            this.ActionType = new System.Windows.Forms.Label();
            this.ListBoxAction = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.LabelActionType = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.labelSaying = new System.Windows.Forms.Label();
            this.labelActorIdx = new System.Windows.Forms.Label();
            this.labelTarget = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1153, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem1,
            this.saveToolStripMenuItem});
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(124, 26);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.textBoxTargetIdx);
            this.tabPage2.Controls.Add(this.textBoxActorIdx);
            this.tabPage2.Controls.Add(this.textBoxSaying);
            this.tabPage2.Controls.Add(this.textBoxDuration);
            this.tabPage2.Controls.Add(this.labelTargetIdx);
            this.tabPage2.Controls.Add(this.buttonRemoveAction);
            this.tabPage2.Controls.Add(this.buttonAddAction);
            this.tabPage2.Controls.Add(this.comboBoxActionType);
            this.tabPage2.Controls.Add(this.ActorIdx);
            this.tabPage2.Controls.Add(this.Saying);
            this.tabPage2.Controls.Add(this.Duration);
            this.tabPage2.Controls.Add(this.ActionType);
            this.tabPage2.Controls.Add(this.ListBoxAction);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1145, 713);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.labelTarget);
            this.panel1.Controls.Add(this.labelActorIdx);
            this.panel1.Controls.Add(this.labelSaying);
            this.panel1.Controls.Add(this.labelDuration);
            this.panel1.Controls.Add(this.LabelActionType);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(492, 336);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(609, 214);
            this.panel1.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "TargetIdx :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "ActorIdx :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Saying :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Duration :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "ActionType :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(-2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "legends";
            // 
            // textBoxTargetIdx
            // 
            this.textBoxTargetIdx.Location = new System.Drawing.Point(598, 251);
            this.textBoxTargetIdx.Name = "textBoxTargetIdx";
            this.textBoxTargetIdx.Size = new System.Drawing.Size(100, 25);
            this.textBoxTargetIdx.TabIndex = 13;
            this.textBoxTargetIdx.TextChanged += new System.EventHandler(this.textBoxTargetIdx_TextChanged);
            this.textBoxTargetIdx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // textBoxActorIdx
            // 
            this.textBoxActorIdx.Location = new System.Drawing.Point(598, 192);
            this.textBoxActorIdx.Name = "textBoxActorIdx";
            this.textBoxActorIdx.Size = new System.Drawing.Size(100, 25);
            this.textBoxActorIdx.TabIndex = 7;
            this.textBoxActorIdx.TextChanged += new System.EventHandler(this.textBoxActorIdx_TextChanged);
            this.textBoxActorIdx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // textBoxSaying
            // 
            this.textBoxSaying.Location = new System.Drawing.Point(598, 136);
            this.textBoxSaying.Name = "textBoxSaying";
            this.textBoxSaying.Size = new System.Drawing.Size(489, 25);
            this.textBoxSaying.TabIndex = 6;
            this.textBoxSaying.TextChanged += new System.EventHandler(this.textBoxSaying_TextChanged);
            // 
            // textBoxDuration
            // 
            this.textBoxDuration.Location = new System.Drawing.Point(598, 81);
            this.textBoxDuration.Name = "textBoxDuration";
            this.textBoxDuration.Size = new System.Drawing.Size(100, 25);
            this.textBoxDuration.TabIndex = 5;
            this.textBoxDuration.TextChanged += new System.EventHandler(this.textBoxDuration_TextChanged);
            this.textBoxDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // labelTargetIdx
            // 
            this.labelTargetIdx.AutoSize = true;
            this.labelTargetIdx.Location = new System.Drawing.Point(466, 251);
            this.labelTargetIdx.Name = "labelTargetIdx";
            this.labelTargetIdx.Size = new System.Drawing.Size(79, 15);
            this.labelTargetIdx.TabIndex = 12;
            this.labelTargetIdx.Text = "TargetIdx";
            // 
            // buttonRemoveAction
            // 
            this.buttonRemoveAction.Location = new System.Drawing.Point(598, 577);
            this.buttonRemoveAction.Name = "buttonRemoveAction";
            this.buttonRemoveAction.Size = new System.Drawing.Size(133, 23);
            this.buttonRemoveAction.TabIndex = 10;
            this.buttonRemoveAction.Text = "RemoveAction";
            this.buttonRemoveAction.UseVisualStyleBackColor = true;
            this.buttonRemoveAction.Click += new System.EventHandler(this.buttonRemoveAction_Click);
            // 
            // buttonAddAction
            // 
            this.buttonAddAction.Location = new System.Drawing.Point(466, 577);
            this.buttonAddAction.Name = "buttonAddAction";
            this.buttonAddAction.Size = new System.Drawing.Size(95, 23);
            this.buttonAddAction.TabIndex = 9;
            this.buttonAddAction.Text = "AddAction";
            this.buttonAddAction.UseVisualStyleBackColor = true;
            this.buttonAddAction.Click += new System.EventHandler(this.buttonAddAction_Click);
            // 
            // comboBoxActionType
            // 
            this.comboBoxActionType.FormattingEnabled = true;
            this.comboBoxActionType.Location = new System.Drawing.Point(598, 27);
            this.comboBoxActionType.Name = "comboBoxActionType";
            this.comboBoxActionType.Size = new System.Drawing.Size(121, 23);
            this.comboBoxActionType.TabIndex = 8;
            this.comboBoxActionType.SelectedIndexChanged += new System.EventHandler(this.comboBoxActionType_SelectedIndexChanged);
            // 
            // ActorIdx
            // 
            this.ActorIdx.AutoSize = true;
            this.ActorIdx.Location = new System.Drawing.Point(466, 192);
            this.ActorIdx.Name = "ActorIdx";
            this.ActorIdx.Size = new System.Drawing.Size(71, 15);
            this.ActorIdx.TabIndex = 4;
            this.ActorIdx.Text = "ActorIdx";
            // 
            // Saying
            // 
            this.Saying.AutoSize = true;
            this.Saying.Location = new System.Drawing.Point(466, 136);
            this.Saying.Name = "Saying";
            this.Saying.Size = new System.Drawing.Size(55, 15);
            this.Saying.TabIndex = 3;
            this.Saying.Text = "Saying";
            // 
            // Duration
            // 
            this.Duration.AutoSize = true;
            this.Duration.Location = new System.Drawing.Point(463, 81);
            this.Duration.Name = "Duration";
            this.Duration.Size = new System.Drawing.Size(71, 15);
            this.Duration.TabIndex = 2;
            this.Duration.Text = "Duration";
            // 
            // ActionType
            // 
            this.ActionType.AutoSize = true;
            this.ActionType.Location = new System.Drawing.Point(463, 27);
            this.ActionType.Name = "ActionType";
            this.ActionType.Size = new System.Drawing.Size(87, 15);
            this.ActionType.TabIndex = 1;
            this.ActionType.Text = "ActionType";
            // 
            // ListBoxAction
            // 
            this.ListBoxAction.FormattingEnabled = true;
            this.ListBoxAction.ItemHeight = 15;
            this.ListBoxAction.Location = new System.Drawing.Point(12, 27);
            this.ListBoxAction.Name = "ListBoxAction";
            this.ListBoxAction.Size = new System.Drawing.Size(404, 574);
            this.ListBoxAction.TabIndex = 0;
            this.ListBoxAction.SelectedIndexChanged += new System.EventHandler(this.ListBoxAction_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1153, 742);
            this.tabControl1.TabIndex = 0;
            // 
            // LabelActionType
            // 
            this.LabelActionType.AutoSize = true;
            this.LabelActionType.Location = new System.Drawing.Point(168, 43);
            this.LabelActionType.Name = "LabelActionType";
            this.LabelActionType.Size = new System.Drawing.Size(55, 15);
            this.LabelActionType.TabIndex = 6;
            this.LabelActionType.Text = "label7";
            // 
            // labelDuration
            // 
            this.labelDuration.AutoSize = true;
            this.labelDuration.Location = new System.Drawing.Point(168, 72);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(55, 15);
            this.labelDuration.TabIndex = 7;
            this.labelDuration.Text = "label8";
            // 
            // labelSaying
            // 
            this.labelSaying.AutoSize = true;
            this.labelSaying.Location = new System.Drawing.Point(168, 105);
            this.labelSaying.Name = "labelSaying";
            this.labelSaying.Size = new System.Drawing.Size(55, 15);
            this.labelSaying.TabIndex = 8;
            this.labelSaying.Text = "label9";
            // 
            // labelActorIdx
            // 
            this.labelActorIdx.AutoSize = true;
            this.labelActorIdx.Location = new System.Drawing.Point(168, 136);
            this.labelActorIdx.Name = "labelActorIdx";
            this.labelActorIdx.Size = new System.Drawing.Size(63, 15);
            this.labelActorIdx.TabIndex = 9;
            this.labelActorIdx.Text = "label10";
            // 
            // labelTarget
            // 
            this.labelTarget.AutoSize = true;
            this.labelTarget.Location = new System.Drawing.Point(168, 166);
            this.labelTarget.Name = "labelTarget";
            this.labelTarget.Size = new System.Drawing.Size(63, 15);
            this.labelTarget.TabIndex = 10;
            this.labelTarget.Text = "label11";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 770);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxTargetIdx;
        private System.Windows.Forms.TextBox textBoxActorIdx;
        private System.Windows.Forms.TextBox textBoxSaying;
        private System.Windows.Forms.TextBox textBoxDuration;
        private System.Windows.Forms.Label labelTargetIdx;
        private System.Windows.Forms.Button buttonRemoveAction;
        private System.Windows.Forms.Button buttonAddAction;
        private System.Windows.Forms.ComboBox comboBoxActionType;
        private System.Windows.Forms.Label ActorIdx;
        private System.Windows.Forms.Label Saying;
        private System.Windows.Forms.Label Duration;
        private System.Windows.Forms.Label ActionType;
        private System.Windows.Forms.ListBox ListBoxAction;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTarget;
        private System.Windows.Forms.Label labelActorIdx;
        private System.Windows.Forms.Label labelSaying;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.Label LabelActionType;
    }
}

