﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using LitJson;
using BlindGameDataDefs;

namespace BlindGamePlotter
{
    public partial class Form1 : Form
    {
        public List<EventDef> EventList;
        public EventDef CurrentEvent;

        private bool bSelecting = false;

        public Form1()
        {
            InitializeComponent();

            EventList = new List<EventDef>();
            CurrentEvent = new EventDef();
        }

        private void textBoxMinDay_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.MinTriggerDay = Convert.ToInt32(textBoxMinDay.Text);
        }

        private void textBoxMaxDay_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.MaxTriggerDay = Convert.ToInt32(textBoxMaxDay.Text);
        }

        private void textBoxMinTrust_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.MinTriggerTrust = Convert.ToInt32(textBoxMinTrust.Text);
        }

        private void textBoxMaxTrust_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.MaxTriggerTrust = Convert.ToInt32(textBoxMaxTrust.Text);
        }

        private void listBoxEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxEvents.SelectedItem == null)
                return;

            CurrentEvent = EventList[listBoxEvents.SelectedIndex];

            bSelecting = true;
            textBoxEventName.Text = CurrentEvent.EventName;
            textBoxMaxDay.Text = CurrentEvent.MaxTriggerDay.ToString();
            textBoxMinDay.Text = CurrentEvent.MinTriggerDay.ToString();
            textBoxMaxTrust.Text = CurrentEvent.MaxTriggerTrust.ToString();
            textBoxMinTrust.Text = CurrentEvent.MinTriggerTrust.ToString();
            textBoxTime.Text = CurrentEvent.WhenInTheDay.ToString("f2");
            textBoxPlace.Text = CurrentEvent.HookName;
            bSelecting = false;
        }

        private void InputNumericCheck_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void buttonAddEvent_Click(object sender, EventArgs e)
        {
            if(listBoxEvents.SelectedItem!=null)
                EventList.Insert(listBoxEvents.SelectedIndex, EventDef.EventClone(CurrentEvent));
            else
                EventList.Add(EventDef.EventClone(CurrentEvent));

            UpdateEventListBox();
        }

        private void buttonRemoveEvent_Click(object sender, EventArgs e)
        {
            if (listBoxEvents.SelectedItem != null)
            {
                EventList.RemoveAt(listBoxEvents.SelectedIndex);
                UpdateEventListBox();
            }
        }

        private void UpdateEventListBox()
        {
            String str = "";

            listBoxEvents.Items.Clear();

            foreach(EventDef eventitr in EventList)
            {
                listBoxEvents.Items.Add(eventitr.EventName);
            }
        }

        private void textBoxEventName_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.EventName = textBoxEventName.Text;

            UpdateEventListBox();
        }

        private string GenerateJson()
        {
            Console.WriteLine("Start GenerateJson==================================");
            string jsonstr;
            StringBuilder GeneratedJsonStr;

            GeneratedJsonStr = new StringBuilder();
            GeneratedJsonStr.Insert(0, "[");

            foreach (EventDef aitr in EventList)
            {
                jsonstr = JsonMapper.ToJson(aitr);
                GeneratedJsonStr.Append(jsonstr);
                if (aitr != EventList.Last())
                    GeneratedJsonStr.Append(",");
            }
            GeneratedJsonStr.Insert(GeneratedJsonStr.Length, "]");

            Console.WriteLine(GeneratedJsonStr.ToString());
            Console.WriteLine("End GenerateJson==================================");

            return GeneratedJsonStr.ToString();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilePath = "";
            SaveFileDialog sfd = new SaveFileDialog();
            //sfd.Filter = "office files(*.doc)|*.doc|Text files(*.txt)|*.txt";
            sfd.Filter = "Blind Game Event Files(*.bp)|*.bp";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径


                FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);

                sw.Write(GenerateJson());
                sw.Close();
                fs.Close();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAll();
            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = (System.IO.FileStream)ofd.OpenFile();
                StreamReader sr = new StreamReader(fs, Encoding.Default);
                string str = sr.ReadToEnd();
                sr.Close();
                fs.Close();
                Console.Write(str);
                EventList = JsonMapper.ToObject<List<EventDef>>(str);
                UpdateEventListBox();
            }
        }

        private void ClearAll()
        {
            EventList = new List<EventDef>();
            CurrentEvent = new EventDef();
            listBoxEvents.ClearSelected();
            listBoxEvents.Items.Clear();
            //UpdateEventListBox();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.WhenInTheDay = float.Parse(textBoxTime.Text);
        }

        private void textBoxPlace_TextChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.HookName = textBoxPlace.Text;
            UpdateEventListBox();
        }

        private void checkBoxTriggerByPlayer_CheckedChanged(object sender, EventArgs e)
        {
            if (bSelecting)
                return;

            CurrentEvent.TriggerByPlayer = checkBoxTriggerByPlayer.Checked;
        }
    }
}
