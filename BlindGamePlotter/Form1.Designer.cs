﻿namespace BlindGamePlotter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxPlace = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxEventName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonRemoveEvent = new System.Windows.Forms.Button();
            this.buttonAddEvent = new System.Windows.Forms.Button();
            this.textBoxMaxTrust = new System.Windows.Forms.TextBox();
            this.textBoxMinTrust = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxEvents = new System.Windows.Forms.ListBox();
            this.textBoxMaxDay = new System.Windows.Forms.TextBox();
            this.textBoxMinDay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxTriggerByPlayer = new System.Windows.Forms.CheckBox();
            this.tabPage3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.checkBoxTriggerByPlayer);
            this.tabPage3.Controls.Add(this.textBoxPlace);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.textBoxEventName);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.textBoxTime);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.buttonRemoveEvent);
            this.tabPage3.Controls.Add(this.buttonAddEvent);
            this.tabPage3.Controls.Add(this.textBoxMaxTrust);
            this.tabPage3.Controls.Add(this.textBoxMinTrust);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.listBoxEvents);
            this.tabPage3.Controls.Add(this.textBoxMaxDay);
            this.tabPage3.Controls.Add(this.textBoxMinDay);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(990, 661);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxPlace
            // 
            this.textBoxPlace.Location = new System.Drawing.Point(541, 196);
            this.textBoxPlace.Name = "textBoxPlace";
            this.textBoxPlace.Size = new System.Drawing.Size(100, 25);
            this.textBoxPlace.TabIndex = 18;
            this.textBoxPlace.TextChanged += new System.EventHandler(this.textBoxPlace_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(453, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 15);
            this.label9.TabIndex = 17;
            this.label9.Text = "HookName";
            // 
            // textBoxEventName
            // 
            this.textBoxEventName.Location = new System.Drawing.Point(145, 196);
            this.textBoxEventName.Name = "textBoxEventName";
            this.textBoxEventName.Size = new System.Drawing.Size(100, 25);
            this.textBoxEventName.TabIndex = 16;
            this.textBoxEventName.TextChanged += new System.EventHandler(this.textBoxEventName_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "EventName";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(271, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "(0.2~0.8)";
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(145, 253);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(100, 25);
            this.textBoxTime.TabIndex = 13;
            this.textBoxTime.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBoxTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 256);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Time";
            // 
            // buttonRemoveEvent
            // 
            this.buttonRemoveEvent.Location = new System.Drawing.Point(319, 47);
            this.buttonRemoveEvent.Name = "buttonRemoveEvent";
            this.buttonRemoveEvent.Size = new System.Drawing.Size(108, 23);
            this.buttonRemoveEvent.TabIndex = 11;
            this.buttonRemoveEvent.Text = "RemoveEvent";
            this.buttonRemoveEvent.UseVisualStyleBackColor = true;
            this.buttonRemoveEvent.Click += new System.EventHandler(this.buttonRemoveEvent_Click);
            // 
            // buttonAddEvent
            // 
            this.buttonAddEvent.Location = new System.Drawing.Point(72, 47);
            this.buttonAddEvent.Name = "buttonAddEvent";
            this.buttonAddEvent.Size = new System.Drawing.Size(89, 23);
            this.buttonAddEvent.TabIndex = 10;
            this.buttonAddEvent.Text = "AddEvent";
            this.buttonAddEvent.UseVisualStyleBackColor = true;
            this.buttonAddEvent.Click += new System.EventHandler(this.buttonAddEvent_Click);
            // 
            // textBoxMaxTrust
            // 
            this.textBoxMaxTrust.Location = new System.Drawing.Point(742, 503);
            this.textBoxMaxTrust.Name = "textBoxMaxTrust";
            this.textBoxMaxTrust.Size = new System.Drawing.Size(100, 25);
            this.textBoxMaxTrust.TabIndex = 9;
            this.textBoxMaxTrust.TextChanged += new System.EventHandler(this.textBoxMaxTrust_TextChanged);
            this.textBoxMaxTrust.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // textBoxMinTrust
            // 
            this.textBoxMinTrust.Location = new System.Drawing.Point(742, 436);
            this.textBoxMinTrust.Name = "textBoxMinTrust";
            this.textBoxMinTrust.Size = new System.Drawing.Size(100, 25);
            this.textBoxMinTrust.TabIndex = 8;
            this.textBoxMinTrust.TextChanged += new System.EventHandler(this.textBoxMinTrust_TextChanged);
            this.textBoxMinTrust.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(634, 506);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "maxTrust";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(634, 439);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "minTrust";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Events";
            // 
            // listBoxEvents
            // 
            this.listBoxEvents.FormattingEnabled = true;
            this.listBoxEvents.ItemHeight = 15;
            this.listBoxEvents.Location = new System.Drawing.Point(145, 303);
            this.listBoxEvents.Name = "listBoxEvents";
            this.listBoxEvents.Size = new System.Drawing.Size(379, 229);
            this.listBoxEvents.TabIndex = 4;
            this.listBoxEvents.SelectedIndexChanged += new System.EventHandler(this.listBoxEvents_SelectedIndexChanged);
            // 
            // textBoxMaxDay
            // 
            this.textBoxMaxDay.Location = new System.Drawing.Point(742, 365);
            this.textBoxMaxDay.Name = "textBoxMaxDay";
            this.textBoxMaxDay.Size = new System.Drawing.Size(100, 25);
            this.textBoxMaxDay.TabIndex = 3;
            this.textBoxMaxDay.TextChanged += new System.EventHandler(this.textBoxMaxDay_TextChanged);
            this.textBoxMaxDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // textBoxMinDay
            // 
            this.textBoxMinDay.Location = new System.Drawing.Point(742, 302);
            this.textBoxMinDay.Name = "textBoxMinDay";
            this.textBoxMinDay.Size = new System.Drawing.Size(100, 25);
            this.textBoxMinDay.TabIndex = 2;
            this.textBoxMinDay.TextChanged += new System.EventHandler(this.textBoxMinDay_TextChanged);
            this.textBoxMinDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumericCheck_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(637, 368);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "PendingDay";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(634, 305);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "MinDay";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 53);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(998, 690);
            this.tabControl1.TabIndex = 0;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1034, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // checkBoxTriggerByPlayer
            // 
            this.checkBoxTriggerByPlayer.AutoSize = true;
            this.checkBoxTriggerByPlayer.Location = new System.Drawing.Point(456, 252);
            this.checkBoxTriggerByPlayer.Name = "checkBoxTriggerByPlayer";
            this.checkBoxTriggerByPlayer.Size = new System.Drawing.Size(149, 19);
            this.checkBoxTriggerByPlayer.TabIndex = 19;
            this.checkBoxTriggerByPlayer.Text = "TriggerByPlayer";
            this.checkBoxTriggerByPlayer.UseVisualStyleBackColor = true;
            this.checkBoxTriggerByPlayer.CheckedChanged += new System.EventHandler(this.checkBoxTriggerByPlayer_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 755);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxEvents;
        private System.Windows.Forms.TextBox textBoxMaxDay;
        private System.Windows.Forms.TextBox textBoxMinDay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox textBoxMaxTrust;
        private System.Windows.Forms.TextBox textBoxMinTrust;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonRemoveEvent;
        private System.Windows.Forms.Button buttonAddEvent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxEventName;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxPlace;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBoxTriggerByPlayer;
    }
}

